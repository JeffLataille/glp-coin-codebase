// Copyright (c) 2009-2015 The Bitcoin Core developers
// Copyright (c) 2015-2017 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "glyph/converter.h"
#include "key.h"
#include "arith_uint256.h"
#include "crypto/common.h"
#include "crypto/hmac_sha512.h"
#include "pubkey.h"
#include "random.h"
#include "util.h"

#include <secp256k1.h>
#include <secp256k1_recovery.h>

extern "C" void SKStructToSKByte (glp_signing_key_t * skStruct, unsigned char sk[512]);
extern "C" void SKByteToSKStruct (glp_signing_key_t * skStruct, const unsigned char sk[512]);


void CKey::MakeNewKey()
{
    for(int i = 0; i < 512; i++) {
        sk[i] = 0;
    }
    glp_signing_key_t skStruct;
    glp_gen_sk(&skStruct);
    SKStructToSKByte(&skStruct, sk);

    fValid = true;
}

CPrivKey CKey::GetPrivKey() const
{
    assert(fValid);
    CPrivKey privkey;
    privkey.resize(SK_SIZE);
    privkey.assign(sk, sk + SK_SIZE);
    return privkey;
}

CPubKey CKey::GetPubKey() const
{

    glp_public_key_t pk;
    glp_signing_key_t skStruct;
    SKByteToSKStruct(&skStruct, sk);
    glp_gen_pk(&pk, skStruct);


    CPubKey pubkey;
    pubkey.structToPK(&pk);
    assert(pubkey.IsValid()); //TODO: this may be deleted because isValid may be deleted
    return pubkey;
}

static bool sigToVector(glp_signature_t sigStruct, unsigned char * vchSig, int * indexPtr) {

  //unsigned char arraySig[MAX_SIG_SIZE]; //the maximum size of signature
  z1ToByteSig(sigStruct.z1, vchSig, indexPtr);
  z2ToByteSig(sigStruct.z2, (vchSig+*indexPtr), indexPtr);
  sparseToByteSig(&(sigStruct.c), (vchSig+*indexPtr), indexPtr);

  return true;


}

static bool vectorToSig(glp_signature_t * sigStruct, const unsigned char * vchSig, int * indexPtr)  {

  //convert arraySig to struct sig
  byteSigToZ1(sigStruct->z1, vchSig, indexPtr);
  byteSigToZ2(sigStruct->z2, (vchSig+ *indexPtr), indexPtr);
  byteSigToSparse(&(sigStruct->c), (vchSig+ *indexPtr), indexPtr);

  return true;
}


bool CKey::Sign(const uint256 &hash, std::vector<unsigned char> &vchSig, uint32_t test_case) const
{
    //turn hash into unsigned char
    unsigned char hashData[32];
    memcpy(hashData, hash.begin(), 32);

    if (!fValid)
        return false;
    vchSig.resize(MAX_SIG_SIZE);
    //signature
    glp_signature_t sigStruct;
    glp_signing_key_t skStruct;
    SKByteToSKStruct(&skStruct, sk);
    if(!glp_sign(&sigStruct, skStruct,(unsigned char *)hashData, 32)){
      LogPrintf("key.cpp: signature creation failed");
      return false;
    }
    //compress signature
    int index = 0;
    int * indexPtr = &index;
    sigToVector(sigStruct, &vchSig[0], indexPtr);
    vchSig.resize(*indexPtr);

    return true;
}

bool CKey::VerifyPubKey(const CPubKey &pubkey) const
{

    unsigned char * rnd = (unsigned char *)calloc(1, 8);
    std::string str = "Bitcoin key verification\n";
    //GetRandBytes(rnd, sizeof(rnd));
    uint256 hash;
    CHash256().Write((unsigned char *)str.data(), str.size()).Write(rnd, sizeof(rnd)).Finalize(hash.begin());
    std::vector<unsigned char> vchSig;
    bool r = Sign(hash, vchSig);
    if(!r) {
        LogPrintf("Tried to verifyPubKey but sign failed (key.cpp)");
        return false;
    }
    bool result = pubkey.Verify(hash, vchSig);
    free(rnd);
    return result;
}

bool CKey::Load(CPrivKey &privkey, CPubKey &vchPubKey, bool fSkipCheck = false)
{
    Set(privkey.begin(), privkey.end());

    fValid = true;
    if (fSkipCheck)
        return true;

    return VerifyPubKey(vchPubKey);
}
