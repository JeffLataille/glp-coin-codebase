/* This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * See LICENSE for complete information.
 */


#include "glp_rand.h"
#include "glp_rand_openssl_aes.h"


// this function creates a poly f such that each coeff is -1, 0 , 1 modulo Q
// recall that the polys in the signing key (s, e) = (s1, s2) are in $R_{q{\rm ,} 1}$

void sample_glp_secret(RINGELT f[GLP_N]){
  uint64_t rand64;
  uint16_t rand2;
  uint16_t rand_bits_used = 0;
  uint16_t i;
  RANDOM_VARS // initialize random number generation
  rand64 = RANDOM64;

#if NISPOWEROFTWO
  for(i = 0; i < GLP_N; i++){
#else
  f[GLP_N-1] = 0;
  for(i = 0; i < GLP_N-1; i++){
#endif

    while (1){
      if(rand_bits_used >= 63){
        rand64 = RANDOM64;
        rand_bits_used = 0;
      }
      rand2 = (uint16_t) rand64 & (uint16_t)3;
      rand64 >>= 2;
      rand_bits_used += 2;
      if (rand2 != 3){    // i.e., break as soon as rand2 is 0, 1, or 2
        break;
      }
    }

    if (rand2 == 0){
      f[i] = 0;
    } else {
      if (rand2 == 1){
        f[i] = 1;
      } else {
        if (rand2 == 2){
	  f[i] = GLP_Q - 1;
        } else {
	  printf("ERROR in secret sampling %d should only equal 0,1 or 2!\n",rand2);
        }
      }
    }
  }
}
