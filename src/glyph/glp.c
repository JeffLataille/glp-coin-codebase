/* This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * See LICENSE for complete information.
 */

#include <string.h>

#include "glp.h"
#include "glp_rand_openssl_aes.h"
#include "glp_utils.h"


/***********************************************SIGNING KEY GENERATION********************************/

/*generates signing key (s1,s2), stored in physical form */
void glp_gen_sk(glp_signing_key_t *sk){
    sample_glp_secret(sk->s1);
    sample_glp_secret(sk->s2);
}


/***********************************************PUBLIC KEY GENERATION********************************/

/*takes a signing key stored in physical space and computes the public key in physical space */
/* the point a is stored in FFT space */
void glp_gen_pk(glp_public_key_t *pk, glp_signing_key_t sk){
  FFT_FORWARD(sk.s1);

  FFT_FORWARD(sk.s2);
  POINTWISE_MUL_ADD(pk->t, a, sk.s1, sk.s2, GLP_N, GLP_Q);
  FFT_BACKWARD(sk.s1);
  FFT_BACKWARD(sk.s2);
  FFT_BACKWARD(pk->t);
}


/**********************************************SIGNING*********************************************/

#if NISPOWEROFTWO
#define N_LIMIT GLP_N
#else
#define N_LIMIT GLP_N-1
#endif

#define MAXSIGNITERS 1000

/*signs a message as (z,c) where z is a ring elt in physical form, and c is a hash output encoded as a sparse poly */
int glp_sign(glp_signature_t *sig,
              const glp_signing_key_t sk,
              const unsigned char *message_digest,
              const size_t dgst_len){
  RINGELT y1[GLP_N]={0}, y2[GLP_N] ={0};
  int success = 0;
  uint16_t i, sign_iters = 0;

  /*initialise random sampling for y1,y2*/
  RANDOM_VARS
  /*sample y1,y2 randomly, and repeat until they pass rejection sampling*/
  while(!success && (sign_iters < MAXSIGNITERS)){
    sign_iters += 1;
#if !NISPOWEROFTWO
    y1[GLP_N-1] = 0;
    y2[GLP_N-1] = 0;
#endif
  for(i = 0; i < N_LIMIT; i++){
    while(1){
      y1[i] = RANDOM32; /*get 32 bits of random */
      y1[i] &= ~(~0u << (B_BITS + 1)); /*take bottom (B_BITS + 1) bits */
      if(y1[i] < 2*GLP_B + 1) break;
    }
    while(1){
      y2[i] = RANDOM32; /*get 32 bits of random */
      y2[i] &= ~(~0u << (B_BITS + 1)); /*take bottom (B_BITS + 1) bits */
      if(y2[i] < 2*GLP_B + 1)   break;
    }
    y1[i] = (y1[i] <=GLP_B )? y1[i] : GLP_Q - (y1[i] -GLP_B );
    y2[i] = (y2[i] <=GLP_B )? y2[i] : GLP_Q - (y2[i] -GLP_B );
  }
  success = glp_deterministic_sign(sig, y1,y2, sk, message_digest, dgst_len);
  }

  return success;
}


/*signs a message for a fixed choice of ephemeral secret y in physcial space
 returns 1 or 0 according to success or failure in doing so (due to rejection sampling)*/
int glp_deterministic_sign(glp_signature_t *signature,
                           const RINGELT y1[GLP_N],
                           const RINGELT y2[GLP_N],
                           const glp_signing_key_t sk,
                           const unsigned char *message_digest,
                           const size_t dgst_len){
  RINGELT u[GLP_N],u_rounded[GLP_N], y1_fft[GLP_N], y2_fft[GLP_N];
  unsigned char hash_output[GLP_DIGEST_LENGTH];
  copy_poly(y1_fft,y1);
  copy_poly(y2_fft,y2);
  FFT_FORWARD(y1_fft);
  FFT_FORWARD(y2_fft);

  /*u = a y1 + y2*/
  POINTWISE_MUL_ADD(u,a,y1_fft,y2_fft,GLP_N,GLP_Q);
  FFT_BACKWARD(u);
  MAPTOCYCLOTOMIC(u,GLP_N,GLP_Q);

  /*round and hash u*/
  copy_poly(u_rounded,u);
  round_poly(u_rounded,GLP_B - OMEGA);
  if(!hash(hash_output, u_rounded, message_digest,dgst_len)) return 0;
  if(!encode_sparse(&(signature->c), hash_output)) return 0;

  // z_1 = y_1 + s_1 c
  sparse_mul(signature->z1, sk.s1, signature->c);
  POINTWISE_ADD(signature->z1,signature->z1,y1,GLP_N,GLP_Q);
  MAPTOCYCLOTOMIC(signature->z1,GLP_N,GLP_Q);

  /*rejection sampling on z_1*/
  for(uint16_t i = 0; i < N_LIMIT; i++) if(ABS(signature->z1[i]) > (GLP_B - OMEGA) ) return 0;

  // z_2 = y_2 + s_2 c
  sparse_mul(signature->z2, sk.s2, signature->c);
  POINTWISE_ADD(signature->z2,signature->z2,y2,GLP_N,GLP_Q);
  MAPTOCYCLOTOMIC(signature->z2,GLP_N,GLP_Q);

  // rejection sampling on z_2
  for(uint16_t i = 0; i < N_LIMIT; i++) if(ABS(signature->z2[i]) > (GLP_B - OMEGA) ) return 0;

  // compression rounding_target = a*z1 - t*c = u - z2
  RINGELT approx_u[GLP_N], rounding_target[GLP_N];
  POINTWISE_SUB(rounding_target,u,signature->z2, GLP_N, GLP_Q);
  MAPTOCYCLOTOMIC(rounding_target,GLP_N,GLP_Q);

  copy_poly(approx_u,rounding_target);
  round_poly(approx_u,GLP_B -OMEGA);

  /*signature compression*/
  /*
  for(uint16_t i = 0; i < N_LIMIT; i++){
    if(approx_u[i] == u_rounded[i]) signature->z2[i] = 0;

    else if(rounding_target[i] <= (GLP_B-OMEGA)){
      if(2*(GLP_Q%(2*(GLP_B-OMEGA) + 1)) >= (GLP_B-OMEGA)){
        if((SIGN(signature->z2[i]) < 0) && (SIGN((rounding_target[i] + signature->z2[i]) %GLP_Q) <0)) signature->z2[i] = NEG(GLP_B-OMEGA);
        else signature->z2[i] = GLP_B-OMEGA;
      }
    }

    else if(rounding_target[i] >= GLP_Q - (GLP_B-OMEGA)){
      if((SIGN(signature->z2[i]) > 0) && (rounding_target[i] + signature->z2[i] >= GLP_Q))signature->z2[i] = (GLP_B-OMEGA);
      else if(approx_u[i] == u_rounded[i] + 1) signature->z2[i] = NEG(GLP_B-OMEGA);
    }

    else if(approx_u[i] == u_rounded[i] + 1) signature->z2[i] = NEG(GLP_B-OMEGA);
    else signature->z2[i] = GLP_B-OMEGA;
  }
  */

  // signature compression rewritten and edited from above code

  for(uint16_t i = 0; i < N_LIMIT; i++){
    //printf("i: %d    signature->z2[%d]: %llu\n", i, i, signature->z2[i]);
    if (approx_u[i] == u_rounded[i]){
      signature->z2[i] = 0;
    } else {
      if (rounding_target[i] <= (GLP_B-OMEGA)){
        //if (2*(GLP_Q%(2*(GLP_B-OMEGA) + 1)) >= (GLP_B-OMEGA)){
          if ((SIGN(signature->z2[i]) < 0) && (SIGN((rounding_target[i] + signature->z2[i]) %GLP_Q) <0)){
	    signature->z2[i] = NEG(GLP_B-OMEGA);
	  } else {
	    signature->z2[i] = GLP_B-OMEGA;
	  }
	  //}
      } else {
	if (rounding_target[i] >= GLP_Q - (GLP_B-OMEGA)){
          if ((SIGN(signature->z2[i]) > 0) && (rounding_target[i] + signature->z2[i] >= GLP_Q)){
	    signature->z2[i] = (GLP_B-OMEGA);
	  } else {
	    //if (approx_u[i] == u_rounded[i] + 1){
	      signature->z2[i] = NEG(GLP_B-OMEGA);
	    //}
	  }
	} else {
	  if (approx_u[i] == u_rounded[i] + 1){
	    signature->z2[i] = NEG(GLP_B-OMEGA);
	  } else {
	    signature->z2[i] = GLP_B-OMEGA;
	  }
	}
      }
    }
    //printf("i: %d    signature->z2[%d]: %llu\n\n", i, i, signature->z2[i]);
  }

  return 1;
}



/**********************************************VERIFICATION*********************************************/

int glp_verify(glp_signature_t sig,
               const glp_public_key_t pk,
               const unsigned char *message_digest,
               const size_t dgst_len){

  RINGELT u[GLP_N],v[GLP_N];
  sparse_poly_t c_test;
  unsigned char hash_output[GLP_DIGEST_LENGTH];
  uint16_t i;

  for(i = 0; i < N_LIMIT; i++){
    if(ABS(sig.z1[i]) > (GLP_B - OMEGA) ) return 0;
    if(ABS(sig.z2[i]) > (GLP_B - OMEGA) ) return 0;
  }


  // \hat{z1}
  FFT_FORWARD(sig.z1);

  // \hat{z2}
  FFT_FORWARD(sig.z2);

  // \hat{u} = \hat{a} \cdot \hat{z1} + \hat{z2}
  POINTWISE_MUL_ADD(u,a,sig.z1,sig.z2,GLP_N,GLP_Q);

  // u = \hat{\hat{u}}
  FFT_BACKWARD(u);

  // sparse multiply: v = t*c
  sparse_mul(v,pk.t,sig.c);

  // subtract: w^\prime = u-v = a*z1+z2-t*c
  // store in u rather than w^\prime
  POINTWISE_SUB(u,u,v,GLP_N,GLP_Q);

  MAPTOCYCLOTOMIC(u,GLP_N,GLP_Q);

  round_poly(u, GLP_B-OMEGA);

  // compute Hash(w^\prime, m) and place result into hash_output
  if (!hash(hash_output, u, message_digest, dgst_len)){
    return 0;
  }

  // convert Hash(w^\prime, m) to c^\prime=PolyHash(w^\prime, m)
  // Note: c^\prime is here denoted as c_test
  if(!encode_sparse(&c_test,hash_output)){
    return 0;
  }

  // check whether c^\prime equals c

  /*
  for (i=0; i<OMEGA; i++){
    printf("%hd   %d   %hd   %d\n", c_test.pos[i], c_test.sign[i], sig.c.pos[i], sig.c.sign[i]);
  }
  */

  for(i = 0; i < OMEGA; i++){
    if(c_test.pos[i] != sig.c.pos[i]) {
        printf("failure at pos:%d: pos:%u c_test:%u\n", i, sig.c.pos[i], c_test.pos[i]);
        return 0;
    }
    if(c_test.sign[i] != sig.c.sign[i]) {
        printf("failure at sign:%d: pos:%u c_test:%u\n", i, sig.c.sign[i], c_test.sign[i]);
        return 0;
     }
  }


  return 1;
}
