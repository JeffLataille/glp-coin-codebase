#include <openssl/rand.h>
#include <openssl/aes.h>
#include <inttypes.h>
#include <assert.h>


// define RANDOM_VARS to be the following list of variable declarations and function commands
#define RANDOM_VARS \
  	AES_KEY aes_key; \
	unsigned char aes_key_bytes[16]; \
	assert (RAND_bytes(aes_key_bytes, 16)!=-1);	\
        AES_set_encrypt_key(aes_key_bytes, 128, &aes_key);	\
	unsigned char aes_ivec[AES_BLOCK_SIZE]; \
	memset(aes_ivec, 0, AES_BLOCK_SIZE); \
	unsigned char aes_ecount_buf[AES_BLOCK_SIZE]; \
	memset(aes_ecount_buf, 0, AES_BLOCK_SIZE); \
	unsigned int aes_num = 0; \
	unsigned char aes_in[AES_BLOCK_SIZE]; \
	memset(aes_in, 0, AES_BLOCK_SIZE);

// define shorthands for obtaining 8, 32 or 64 bits of random
#define RANDOM8   ((uint8_t) randomplease(&aes_key, aes_ivec, aes_ecount_buf, &aes_num, aes_in))
#define RANDOM32 ((uint32_t) randomplease(&aes_key, aes_ivec, aes_ecount_buf, &aes_num, aes_in))
#define RANDOM64 ((uint64_t) randomplease(&aes_key, aes_ivec, aes_ecount_buf, &aes_num, aes_in))


uint64_t randomplease(AES_KEY *aes_key, unsigned char aes_ivec[AES_BLOCK_SIZE],
                      unsigned char aes_ecount_buf[AES_BLOCK_SIZE],
                      unsigned int *aes_num, unsigned char aes_in[AES_BLOCK_SIZE]);


// Notes:

// RAND_bytes(aes_key_bytes, 16) puts 16 cryptographically strong pseudo-random bytes into aes_key_bytes[]. An error occurs if the PRNG (i.e., pseudo-random number generator) has not been seeded with enough randomness to ensure an unpredictable byte sequence.

// the actual key is the 128-bit string in aes_key_bytes, but this function transforms it (for efficiency) into a new key stored in the structure aes_key
