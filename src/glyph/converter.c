/* This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * See LICENSE for complete information.
 */


#include "glp.h"
#include <stdio.h>
#include <stdlib.h>
#include "glp_utils.h"
#include "converter.h"

#define GETBIT(p, i) (((p)[(i)>>3] >> ((i)& 7) )& 1)
#define GETBIT16(p, i) (((p)[(i)>>4] >> ((i)& 15) )& 1)
#define GETBIT32(p, i) (((p)[(i)>>5] >> ((i)& 31) )& 1)
#define FLIPBIT(p, i) do { (p)[i>>3] ^=(1 <<  ((i) & 7)); } while (0)
#define FLIPBIT16(p, i) do { (p)[i>>4] ^=(1 <<  ((i) & 15)); } while (0)
#define FLIPBIT32(p, i) do { (p)[i>>5] ^=(1 <<  ((i) & 31)); } while (0)

void SKStructToSKByte (glp_signing_key_t * skStruct, unsigned char sk[512]) {
  int byteCounter = 0;
  //define bit counter (ex. it is 7 when we wrote the 7th bit of the current byte)
  int bitCounter = 0;
  //for every byte in sk,
  uint32_t coeff = 0;
  RINGELT * coefPtr;
  for(int j = 0; j < 2; j++) {
      if(j == 0) {coefPtr = skStruct->s1;}
      else {coefPtr = skStruct->s2;}
      for(int i = 0; i < GLP_N; i++) {
          coeff = coefPtr[i];
          if(coeff == 65536) {
        	    //write a 10 at the bit location
        	    FLIPBIT(&(sk[byteCounter]), bitCounter);
        	    bitCounter++;
              //if bitCounter == 8, set it = 0 and increment byteCounter
        	    if(bitCounter == 8) {
                  bitCounter = 0;
                  byteCounter++;
              }
              bitCounter++;
              if(bitCounter == 8) {
                  bitCounter = 0;
                  byteCounter++;
              }
          }
          else if(coeff == 0) {
              //write 00 to the bit location
              bitCounter += 2;
              if(bitCounter == 8) {
                  bitCounter = 0;
                  byteCounter++;
              }
              else if(bitCounter == 9) {
                  bitCounter = 1;
                  byteCounter++;
              }
          }
          else if(coeff == 1) {

              bitCounter++;
              if(bitCounter == 8) {
                  bitCounter = 0;
                  byteCounter++;
              }
              FLIPBIT(&(sk[byteCounter]), bitCounter);
              bitCounter++;
              if(bitCounter == 8) {
                  bitCounter = 0;
                  byteCounter++;
              }
          }
      }
    }
}

void SKByteToSKStruct(glp_signing_key_t * skStruct, const unsigned char sk[512]) {
  int bitCounter = 0;
  int byteCounter = 0;
  unsigned char currByte;
  unsigned char * currBytePtr = &currByte;
  int i = 0;
  RINGELT * si;
  //iterate through bytes for 2048 elements

  for(int j = 0; j < 2; j++) {
      if (j == 0) {
          si = skStruct->s1;
      }
      else if (j == 1) {
          si = skStruct->s2;
      }
      for(int i = 0; i < 1024; i++) {
          if(GETBIT(sk, (i*2)+(j*2048))) {
              if(!GETBIT(sk, (i*2+1)+(j*2048))) {
                  si[i] = 65536;
              }
          }
          else {
              if(GETBIT(sk, (i*2+1)+(j*2048))) {
                  si[i] = 1;
              }
              else {
                  si[i] = 0;

              }

          }
      }

  }

   // for(int k = 0; k < 512; k++ ) {printf("byte %d: %d\n", k, skStruct->s2[k]); }

}

void PKStructToPKByte (RINGELT pk[GLP_N], unsigned char cPK[2176]) {

    int b = 0;
    for(int i = 0; i < 1024; i++) {
        for(int j = 0; j < 17; j++) {
            b = GETBIT32(&pk[i], j);
            if(b) {
                FLIPBIT(&cPK[0], ((i*17)+j));
            }
        }
    }

}

void PKByteToPKStruct(RINGELT pk[GLP_N], const unsigned char cPK[2176]) {

    int b = 0;
    for(int i = 0; i < 1024; i++) {
        for(int j = 0; j < 17; j++) {
            b = GETBIT(&cPK[0], (i*17+j));
            if(b) {
                FLIPBIT32(&pk[i], j);
            }
        }
    }

}

void z2ToByteSig(RINGELT z2[GLP_N], unsigned char cZ2[256], int * indexPtr) {

  int byteCounter = 0;
  //define bit counter (ex. it is 7 when we wrote the 7th bit of the current byte)
  int bitCounter = 0;
  //for every byte in sk,
  uint32_t coeff = 0;

  for(int i = 0; i < GLP_N; i++) {
      coeff = z2[i];
      if(coeff == 0) {

    	    //write a 1 at the bit location
    	    FLIPBIT(&(cZ2[byteCounter]), bitCounter);
    	    bitCounter++;
          //if bitCounter == 8, set it = 0 and increment byteCounter
    	    if(bitCounter == 8) {
              bitCounter = 0;
              byteCounter++;
          }
      }
      else if(coeff == 16367) {
          //write 00 to the bit location
          bitCounter += 2;
          if(bitCounter == 8) {
              bitCounter = 0;
              byteCounter++;
          }
          else if(bitCounter == 9) {
              bitCounter = 1;
              byteCounter++;
          }
      }
      else if(coeff == 49170) {

          bitCounter++;
          if(bitCounter == 8) {
              bitCounter = 0;
              byteCounter++;
          }
          FLIPBIT(&(cZ2[byteCounter]), bitCounter);
          bitCounter++;
          if(bitCounter == 8) {
              bitCounter = 0;
              byteCounter++;
          }
      }
  }

  //deincrement only if there is no data stored in the byte
  if(bitCounter != 0) {
      byteCounter++;
  }
  //indexPtr is the end of the z2, not the byte after the end
  *indexPtr += byteCounter;

}

void byteSigToZ2(RINGELT z2[GLP_N], const unsigned char cZ2[256], int * indexPtr) {
    int bitCounter = 0;
    int byteCounter = 0;
    unsigned char currByte;
    unsigned char * currBytePtr = &currByte;
    int i = 0;

    //iterate through bytes for 1024 elements
    while(i < GLP_N) {
        *currBytePtr = cZ2[byteCounter];
        GETBIT(currBytePtr, bitCounter);
        //if bit is 1 then z2[i] is a 0
        if(GETBIT(currBytePtr, bitCounter)) {
//            printf("1: %d, %d\n", *currBytePtr, bitCounter);
            z2[i] = 0;
	          i++;
            bitCounter++;
            if(bitCounter == 8) {
                byteCounter++;
                bitCounter = 0;
            }
        }
        //if bit is 0, check next bit
        else {
            bitCounter++;
            if(bitCounter == 8) {
                byteCounter++;
                bitCounter = 0;
                *currBytePtr = cZ2[byteCounter];
            }
            if(GETBIT(currBytePtr, bitCounter)) {
//                printf("49: %d, %d\n", *currBytePtr, bitCounter);
                z2[i] = 49170;
	        i++;
 	        bitCounter++;
            }
            else {
//                printf("16: %d, %d\n", *currBytePtr, bitCounter);
                z2[i] = 16367;
                i++;
                bitCounter++;
            }
            if(bitCounter == 8) {
                byteCounter++;
                bitCounter = 0;
            }
        }
    }
    if(bitCounter != 0) {
        byteCounter++;
    }
    *indexPtr += byteCounter;
}


void byteSigToZ1(RINGELT z1[GLP_N], const unsigned char cZ1[1920], int * indexPtr) {
    //remember, first is least (cZ1[0] is 7 bits)
    unsigned char * ucZ1 = (unsigned char * )calloc(1, 2048);

    int b = 0;
    for(int i = 0; i < 1024; i++) {
        for(int j = 0; j < 15; j++) {
            b = GETBIT(&cZ1[0], (i*15+j));
            if(b) {
                FLIPBIT(&ucZ1[i*2], j);
            }
        }
    }

    uint32_t diff;
    for(size_t i = 0; i < 2048; i+=2) {
        uint16_t * coef = (uint16_t*) &(ucZ1[i]);
        //printf("coef %u\n", *coef);
        z1[i/2] = (uint32_t) *coef;
        //printf("\nz1[i]: %d", z1[i/2]);
        if(z1[i/2] >= 16368) {
            diff = z1[i/2] - 16368;
            z1[i/2] = 49170 + diff;
        }
    }
    *indexPtr += 1920;

    free(ucZ1);
}


void z1ToByteSig(RINGELT z1[GLP_N], unsigned char cZ1[1920], int * indexPtr) {
    //for each 32 int
        //if int = 0-16367 then take in, put it in 16 bit int, and bitshift 1 to the left.
        //the 16th bit is where next term starts.
        //if int = 49170-65536 then substract x-49170 and + 16368, put in 16 bit in.
        //Then bitshift it all 1 to left.

    unsigned char * ucZ1 = (unsigned char * )calloc(1, 2048);
    uint32_t diff;
    for (size_t i = 0; i < 2048; i+=2) {
        if(z1[i/2] <= 16367) {
            uint16_t * int16 = (uint16_t*)&(ucZ1[i]);
            *int16 = (((uint16_t)z1[i/2])); //I might need to do the second cast in a step before this loop
        }
        else if(z1[i/2] >= 49170) {
            diff = z1[i/2] - 49170;
            uint16_t * int16 = (uint16_t*)&(ucZ1[i]);
            *int16 = ((uint16_t)(diff+16367+1)); //<< 1
        }
    }

    int b = 0;
    for(int i = 0; i < 1024; i++) {
        for(int j = 0; j < 15; j++) {
            b = GETBIT(&ucZ1[2*i], j);
            if(b) {
                FLIPBIT(&cZ1[0], ((i*15)+j));
            }
        }
    }

    *indexPtr += 1920;

    free(ucZ1);
}

void sparseToByteSig(sparse_poly_t * c, unsigned char cc[22], int * indexPtr) {

    //for each term in poly
    for(int i = 0; i < 16; i++) {
        //set next 10 bits to the coeff
        for(int j = 0; j < 10; j++) {
            if(GETBIT16(&(c->pos[i]), j)) {
               FLIPBIT(&(cc[0]), ((i*11)+j));
            }
        }
        //set 11th bit to sign
        if(c->sign[i] == 1) {
            FLIPBIT(&(cc[0]), ((i*11) + 10));
        }
    }
    *indexPtr += 22;
}

void byteSigToSparse(sparse_poly_t * c, const unsigned char cc[22], int * indexPtr) {

    for(int i = 0; i < 16; i++) {
        for(int t = 0; t < 10; t++) {
            if(GETBIT(&(cc[0]), (11*i)+t)) {
                FLIPBIT16(&(c->pos[i]), t);
            }
        }
        if(GETBIT(&(cc[0]), ((11*i) + 10))) {
            c->sign[i] = 1;
        }
        else {
            c->sign[i] = 0;
        }
    }
    *indexPtr += 22;
}
