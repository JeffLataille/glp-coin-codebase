
What is GLP coin?
=====================================

GLP coin is a quantum resistant fork of Bitcoin Cash (Bitcoin Unlimited implementation). The quantum resistant algorithm used is called Glyph [https://eprint.iacr.org/2017/766.pdf], which is a variant of GLP. Glyph was chosen over other quantum resistant algorithms because it has a smaller combined signature and public key size than other schemes, such as Winternitz OTS and Ring-TESLA. For this reason, Glyph can fit more transactions in a block than any other quantum resistant coin.


Developers
=====================================
Envieta Systems: Joseph Netti, Jeffery Lataille, Paul Li 
Bitcoin Core, Bitcoin Unlimited, and Satoshi Nakamoto (for original codebase)


Installation
=====================================

Building from source:

1. sudo apt-get install automake autoconf libtool

2. sudo apt-get install git build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils libboost-all-dev

3. Berkley db:

    sudo add-apt-repository ppa:bitcoin-unlimited/bu-ppa
    
    sudo apt-get update
    
    sudo apt-get install libdb4.8-dev libdb4.8++-dev

4. For gui (optional)

    sudo apt-get install libqt4-dev libprotobuf-dev protobuf-compiler libqrencode-dev

5. ./autogen.sh

6. ./configure:

    Without gui: ./configure --with-gui=no --disable-tests --enable-debug --disable-bench
    
    With gui: ./configure --with-gui=yes --disable-tests --enable-debug --disable-bench
    
    With gprof: --enable-gprof
    
    To see more options: ./configure --help

7. make

To run:

   Daemon: ./src/bitcoind
   
   GUI: ./src/qt/bitcoin-qt

Testing guide
=====================================
Data directory: linux: cd .bitcoin 
Insert bitcoin.conf into data directory (if using regtest, put it in .bitcoin/regtest). 
A pretty indepth overview, although it is DOESN'T cover the bitcoin cash additions: https://en.bitcoin.it/wiki/Running_Bitcoin 
Here is small example bitcoin.conf:

listen=1
server=1
maxuploadtarget=200

#change with your username and password
rpcuser=username
rpcpassword=password

use-thinblocks=true
peerbloomfilters=true

#use connect=IP if you want to connect directly to a node. 
#ex. connect=192.168.200.140
#use whitelist=IP to make sure an IP is never banned

#regtest=1 for regtest

--------------------------------

If you want to test the code, enable regtest=1 in the bitcoin.conf (this will create a private tester).
Then after building, run ./bitcoind. bitcoind is the background process that runs the coin (it is called bitcoind out of convention, but it might be changed in the future). To create transactions and blocks and do other things you use the client. 

Client: ./bitcoin-cli command
For list of commands: ./bitcoin-cli -regtest help

Example commands: 
create a new block: ./bitcoin-cli generatetoaddress 1 [minerAddress]
send glyph: ./bitcoin-cli sendtoaddress [recipientAddress] [amount]

